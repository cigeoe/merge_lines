# CIGeoE Identify Dangles

This plugin merge 2 lines that overlap (connected in a vertex) and have same attribute names and values.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_merge_lines” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Merge Lines”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_merge_lines” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - Using the "Select Features" tool, select the two features ("line geometry") to be merged.

![ALT](/images/image1.png)
![ALT](/images/image2.png)
![ALT](/images/image3.png)

2 - Click on plugin icon:

![ALT](/images/image4.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
